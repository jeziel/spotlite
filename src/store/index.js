import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    termHistory: [],
    currentTermPosition: -1,
  },
  mutations: {
    addTermToHistory(state, term) {
      if (!term || state.termHistory.includes(term)) {
        return;
      }

      state.termHistory.push(term);
    },
    setCurrentTermPosition(state, termPosition) {
      // TODO. use getter?
      if (state.termHistory.length < 1) {
        state.currentTermPosition = -1;
        return;
      }

      if (termPosition < 0) {
        state.currentTermPosition = state.termHistory.length - 1;
        return;
      }

      if (termPosition >= state.termHistory.length) {
        state.currentTermPosition = 0;
        return;
      }
    }
  },
  actions: {
    previousTerm({state, getters, commit}) {
      commit('setCurrentTermPosition', state.currentTermPosition - 1)
    },
    nextTerm({state, getters, commit}) {
      commit('setCurrentTermPosition', state.currentTermPosition + 1)
    },
  },
  getters: {
    historyIsEmpty(state) {
      return state.termHistory.length < 1;
    },
    noSelectedHistoryTerm(state) {
      return state.currentTermPosition < 0;
    },
    currentHistoryTerm(state, getters) {
      if (getters.historyIsEmpty || getters.noSelectedHistoryTerm) {
        return '';
      }
      return state.termHistory[state.currentTermPosition];
    },
  },
});
