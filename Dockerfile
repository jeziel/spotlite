FROM node:slim

MAINTAINER Tundraka <tundraka@gmail.com>

RUN npm install --quiet --global \
      @vue/cli \
      @vue/cli-service-global

RUN mkdir /code
#COPY . /code

WORKDIR /code
